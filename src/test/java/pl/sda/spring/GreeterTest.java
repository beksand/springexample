package pl.sda.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.PrintStream;

import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BeanTestConfiguration.class)
public class GreeterTest {

    @Autowired
    private Greeter greeter;

    @Autowired
    private PrintStream out;

    @Test
    public void shouldGreet(){

        greeter.greet("Bro");

        verify(out).println("Hej Bro in GreeterApp 1.0");
    }
}
