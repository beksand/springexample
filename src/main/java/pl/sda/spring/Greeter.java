package pl.sda.spring;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.OutputStream;
import java.io.PrintStream;

public class Greeter {

    @Autowired
    private String appName;

    @Autowired
    private PrintStream out;

    public  void greet(String who){
        out.println("Hej "+ who +" in "+appName);
    }

}
