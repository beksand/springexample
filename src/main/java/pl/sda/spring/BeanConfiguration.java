package pl.sda.spring;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.PrintStream;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("singleton")
    public Greeter greeter(){
        return new Greeter();
    }

    @Bean
    public String appName(){
        return "GreeterApp 1.0";
    }

    @Bean
    public PrintStream out(){
        return System.out;
    }
}
