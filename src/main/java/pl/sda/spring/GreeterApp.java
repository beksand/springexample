package pl.sda.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class GreeterApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(BeanConfiguration.class);
        Greeter greeter = (Greeter) context.getBean("greeter");
        greeter.greet("Bro");
        context.close();
    }
}
